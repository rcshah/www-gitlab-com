---
layout: handbook-page-toc
title: "Employment Law at GitLab"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
Employment law refers to the law that governs the relationship between employer and employee, so the Employment section of GitLab Legal interacts with the People Group on a regular basis to provide information and legal advice related to the entire arc of our team members’ relationship with GitLab.  **So what does that mean?**


- It means we work closely with the Tax and People Operations team to determine scalable employment solutions for the Company as it hires across the globe.
- It means we support the recruiting department in meeting its goals and ensuring compliance with all local laws and regulations as they source, recruit, and hire new team members.
- It means we help the People Operations team onboard new team members and get them the information they need that applies both to their location and to their job responsibilities.
- It means we support the Total Rewards group in developing policies that meet GitLab’s needs, our team members’ needs, and local requirements.
- It means we support our People Business Partners and Total Rewards as they manage team member relations, performance, requests for accommodation or adjustments, promotions, transfers, and any other types of issues that pop up during the course of the relationship.
- It means we support our Diversity, Inclusion, and Belonging team in ensuring that decisions across the entire arc of the relationship match our mission statement.
- And it means we provide support when it comes to end of employment decisions,  offboarding, and any follow-up matters that can occur.

GitLab’s employment attorney provides information regarding legal risk and compliance requirements based on particular jurisdictions and particular job types. Often, it is up to the others within the organization whether or not to continue down a certain path once the risks and requirements are known.  

## Working with Legal on Employment Issues
When individual team members have questions about their employment, they should interact with the People Group rather than reaching out directly to the Legal team. Visit the People Group Handbook to determine who to work with for your particular needs.

If People Group needs to engage with Legal on a particular question, they will reach out to Legal staff via DMs in Slack or via email in order to ensure that details of the situation and team member personal information remain confidential. This respects employee privacy and retains attorney-client privilege.

Below are some common employment topics where Legal is asked to provide input. The paragraphs below are intended to assist People Group to know when to reach out to Legal.

**Note: GitLab team members should *always* obtain approval from the Director of Legal, Employment prior to speaking with outside counsel regarding employment-related matters.**

### Before Hire:
**Recruiting.** Legal reviews sourcing and recruiting strategy for compliance and advises about requirements for applicant tracking in the various countries in which we hire.

**Background Check.** Legal reviews overall background check policy and procedure in place, helps develop background check adjudication process, and serves as escalation point when PBP responsible for reviewing background check has questions or concerns about specific results.

### During Hire
**Contract review.** Legal should be asked to review negotiated contracts that seek to change standard terms. 

**Conversions.** As GitLab expands our international presence through new entities, some current contractors may become GitLab employees. Legal assists with this process.

**Compliance with onboarding issues.** As different countries have different requirements for hiring and disclosures, Legal will assist People Group to ensure that onboarding issues are compliant for specific locales.

### During Employment
**Total Rewards.** People Group is responsible for determining what benefits should be provided. Legal is available for advice when determining whether those benefits comply with local law, how benefits will be communicated, acknowledged via signature, and tracked.

**Relocation request reviews.** Relocation requests are handled by People Group, which has a very thorough explanation of the process and requirements (https://about.gitlab.com/handbook/people-group/contracts-and-international-expansion/#relocation) in the People Group Handbook. After People Group team members have worked through their workflow, Legal is available to assist with specific unresolved questions.

**Conflicts of Interest.** If a GitLab team member engages People Group about a possible conflict of interest, the PBP should encourage discussion between the team member and their manager, as many conflict of interest determinations are business decisions, rather than legal decisions. If the team member and manager agree that there is no conflict then the applicable E-Group member can provide written approval, which is maintained in Bamboo HR.  Legal is involved in managing the process to ensure that decisions regarding what is or is not a conflict remain consistent across departments and so that the manager and team member can understand the potential risks and implications in order to make an informed decision.

### General Business Needs involving Employment Issues
**Scalable employment solutions.** As GitLab continues to grow, our systems must be scalable. GitLab Legal can assist in developing solutions that ease the work burden on People Group while maintaining compliance with legal requirements.

**Policy development.** Legal should be asked to perform a final review when policies are updated or created.

**Contract negotiations.** In the event that a customer or vendor has questions regarding terms that affect our team members (such as drug tests or more intensive background checks), the Contract team will reach out to the Employment team.  
