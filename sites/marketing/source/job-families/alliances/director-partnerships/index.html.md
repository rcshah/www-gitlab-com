---
layout: job_family_page
title: "Director of Partnerships"
---

## Job Grade

The Director, Partnerships is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

## Responsibilities

* Identify and cultivate strategic relationships with current and potential technology partners and large enterprises.
* Develop (and act on) collaboration plans for key partners, and develop product adoption and customer success plans.
* Lead and develop differentiated relationships between GitLab and our technology partners' teams. This includes partners executive leaders, product managers, engineering, sales and marketing.
* Act as a liaison between internal teams at GitLab and strategic partners to communicate about feature requests, product roadmap, and how the alliance influences timing and direction.
* Understand the relative strength of competitors within strategic partners, if any.
* Evaluate effectivity of partner relationships and engagements in delivering positive ROI.
* Speak regularily in public as the voice for GitLab and the partner
* SA level expertise with GitLab AND the partners technology

## Requirements

* You know the DevOps software ecosystem like the back of your hand.
* You’re comfortable with code, and enjoy talking with developers and engineering directors alike.
* You’ve shown that you can build long term (international) strategic relationships, preferably involving open source software.
* You share our [values](/handbook/values/), and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#director-group)
* Ability to use GitLab

## Career Ladder

The next step in the Director, Partnerships job family is not yet defined at GitLab. 

## Hiring Process
TODO
