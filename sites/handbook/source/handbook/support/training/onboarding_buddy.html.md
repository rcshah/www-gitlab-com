---
layout: handbook-page-toc
title: Support Onboarding Buddy
description: How to be an onboarding buddy to a new Support Engineer
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Support Onboarding Buddy

You've become an onboarding buddy to a new Support Engineer. Congratulations! You've been granted a pivotal role in helping someone new learn about how to best contribute to our customers, our team, and our product.

## Responsibilities

These responsibilities of the Support onboarding buddy are an extension of [the GitLab Onboarding Buddy responsibilities](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-buddies/#buddy-responsibilities), so please review that page first.

1. Help the new Support Engineer through any sticking points they have in their onboarding module.
1. Communicate often with the new hire and either help or direct them to get help when stuck.
1. Help the new Support Engineer adapt to and learn the role by having frequent calls and pairing sessions to learn about its day-to-day duties.

## Structure

During your time as onboarding buddy, you'll be working alongside the new Support Engineer while they complete their onboarding. Consider the timeframes below to be estimations: the pace will really be determined by the new Support Engineer.

| Week (estimated)     | Task |
| ----------- | ----------- |
| [Weeks 1-2](#first-1-2-weeks) | Call to meet the new Support Engineer, help them get acclimated, answer any general questions they may have, discuss schedules, and share anything else they may find useful as they get started.       |
|   | Open the lines of communication in Slack. Throughout this process, check in with them periodically to see how their onboarding module is progressing and ask if they need help.        |
| |Call to help them update the team page, support-resources, dev-resources, the GDK, or anything else they may have questions about or trouble with. |
| [Weeks 3+](#pairings)| Pair at least twice, but more often if you can. Ideally, aim for once a week. |
| | Invite them to shadow you on any customer calls. |

## Ideas & Suggestions

Note that everyone's needs are different, so consider the following as a list of ideas to customize to their individual needs. Not everyone needs everything from the below lists, but everyone would find at least a few of the following helpful.

### First 1-2 weeks

  - Meet and get to know each other
  - Answer general questions
  - Talk about your “schedule” and how you work every day - walk them through a few example days
  - Talk about how to use tools like Slack, Expensify, etc.
  - Show them some helpful Slack channels
  - Which Slack channels to keep track of daily
  - Which Google docs to keep track of (SWIR, etc.)
  - Show them some helpful handbook pages to read during onboarding
  - Show them the GitLab architecture diagrams
  - Show them some product or support-team-meta issues and clarify that they can contribute to anything
  - Remind them they can get reimbursed for any books or training, and show them the “spending company money” page. If it’s expensive, talk to their manager first.
  - Show them how to use dev-resources or support-resources
  - Help them update the team page with their info (one of their Onboarding Issue checklist items)

### Pairings

- Introduction to Zendesk and how we use it:
  - Walk them through Zendesk and how to use it
  - Answer any questions they have about the ticket workflow handbook pages
  - Walk through your process for choosing and answering tickets
  - Talk about [setting a signature](https://gitlab.com/gitlab-com/support/team/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&search=salutation)
  - Show them how to create a ticket pairing issue

- Pair on tickets:
  - Share your screen and answer a few easy tickets with them
  - If they brought any tickets to the call, answer those first
  - Alternate who shares their screen and answers the tickets
  - Reproduce problems whenever you can. Show them how to quickly spin up an instance using your preferred method (Docker, support-resources, GCP, etc.)

- Customer calls:
  - Ping them when you have a customer call so they can shadow it
  - Show them how to find customer calls to shadow
  - Be available for their first customer call, if possible. If not, do what you can to ensure they have a shadow to help them out


### What to do afterwards

- Over the first few months, if you have anything interesting you’re working on that you think they can learn from, ping them and see if they’d like to either pair or shadow you on it.
