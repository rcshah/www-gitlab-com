---
layout: handbook-page-toc
title: "Demo Systems Tutorials"
---

## On this page
{:.no_toc}

- TOC
{:toc}

If you have issues, please post in the `#demo-systems` channel on Slack for assistance. You can also create issues for feature requests or reporting bugs in [Demo Feature Requests and Projects](https://gitlab.com/groups/gitlab-com/customer-success/demo-systems/demo-feature-requests/-/issues).

## [Getting Started](/handbook/customer-success/demo-systems/tutorials/getting-started)

* [Creating and accessing your account](/handbook/customer-success/demo-systems/tutorials/getting-started/creating-accessing-your-account)
* [Configuring GitLab with group-level Kubernetes cluster](/handbook/customer-success/demo-systems/tutorials/getting-started/configuring-group-cluster)
* [Using project templates and sample projects](/handbook/customer-success/demo-systems/tutorials/getting-started/using-templates-sample-projects)
* [Using the demo designer for creating sample data](/handbook/customer-success/demo-systems/tutorials/getting-started/using-demo-designer)

<!--
## [Advanced Use Cases](/handbook/customer-success/demo-systems/tutorials/advanced-use-cases)

* Using GitLab Pages
-->

<!--
## [CI/CD and Auto DevOps](/handbook/customer-success/demo-systems/tutorials/ci-cd)

* Using Auto DevOps
* Configuring your own runner
-->

## [Integrations](/handbook/customer-success/demo-systems/tutorials/integrations)

* [Create a Jenkins pipeline](/handbook/customer-success/demo-systems/tutorials/integrations/create-jenkins-pipeline)
* Using the JIRA integration

<!--
## [Contributed Tutorials](/handbook/customer-success/demo-systems/tutorials/contributed)

* No tutorials available
-->