---
layout: handbook-page-toc
title: "TPM.1.04 - Vendor Compliance Monitoring Control Guidance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# TPM.1.04 - Vendor Compliance Monitoring

## Control Statement

Maintain a program to monitor service providers’ compliance status at least annually.

## Context
We need to validate a third party's compliance with their stated security posture on a yearly basis to ensure that any changes to their information security controls does not have a material impact on organization's compliance requirements. The stated security posture is that which is evaluated by Security Compliance during the initial security review performed as part of the procurement process. This will assist in obtaining new customers and help maintain assurance with our current customers. This control can be tested by reviewing the Procure to pay process in the handbook and vendor compliance monitoring process in the compliance repo runbook. Testing this control, implies to ensure that GitLab maintains a program to monitor service providers’ compliance status at least annually. This can be achieved by providing the following evidences to satisfy this control: 
* Copies of the Soc Report Review issues of the in-scope third-party applications 
* Step-by step process of the vendor compliance review followed 
* Evidence of a compliance issue documenting our review of their security reports 

All the above evidences can be gathered via various links pertaining to the handbook page, Merge Requests, issues opened in reference to and in adherence to this control.

## Scope
All third party service providers that fall within the GitLab Control Framework (GCF).  

## Ownership
Control Owner: `Security Compliance`
Process Owner:  Security Compliance

## Additional control information and project tracking
Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Vendor Compliance Monitoring control issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/issues/925).

### Policy Reference
*  [Vendor compliance monitoring process in the Compliance repo runbook](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/blob/master/runbooks/Vendor_Security_Report_Review.md)
*  [PCI-DSS SAQ-A 12.8.4 - Monitor Service Providers' PCI DSS Compliance Status](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/issues/100)
*  [Procure to Pay section of the handbook](/handbook/finance/procure-to-pay/#step-3-authorizations) 


## Framework Mapping
* PCI
  * 12.8.4
