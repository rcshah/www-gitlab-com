---
layout: handbook-page-toc
title: "SA Career Development"
description: "For career development Solution Architects can choose between an individual contributor or leadership track"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

[**SA Practices**](/handbook/customer-success/solutions-architects/sa-practices) - [**Sales Plays**](/handbook/customer-success/solutions-architects/sales-plays) - [**Tools and Resources**](/handbook/customer-success/solutions-architects/tools-and-resources) - [**Career Development**](/handbook/customer-success/solutions-architects/career-development) - [**Demonstration**](/handbook/customer-success/solutions-architects/demonstrations) - [**Processes**](/handbook/customer-success/solutions-architects/processes)

## Career Development

Solution Architects can choose between an Individual Contributor track with various levels which is highlighted in the [Solution Architect Individual Contributor Levels](https://about.gitlab.com/job-families/sales/solutions-architect/#solutions-architect).

Solutions Architects can also pursue a leadership track highlighted [Solution Architect Manager Levels](https://about.gitlab.com/job-families/sales/solutions-architect/#manager-solutions-architects)

Solution Architects can choose between the following specialities:

[Channel Solution Architect](https://about.gitlab.com/job-families/sales/solutions-architect/#channel-solution-architect)

[Alliances Solution Architect](https://about.gitlab.com/job-families/sales/solutions-architect/#alliances-solution-architect)

[Public Sector Solutions Architect](https://about.gitlab.com/job-families/sales/solutions-architect/#public-sector-solutions-architect)

[Commercial Solution Architect](https://about.gitlab.com/job-families/sales/solutions-architect/#commercial-solutions-architect)

Solution Architects are hired through a hiring process [Solution Architect hiring process](https://about.gitlab.com/job-families/sales/solutions-architect/#hiring-process)


[TODO](<TODO>)Solution Architect promotion process is listed here

